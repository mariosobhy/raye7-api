require 'spec_helper'

RSpec.configure do |config|
  config.before(:example) {
    @group = Group.new(name: "new")
  }
end

RSpec.describe Group, type: :model do
  context "when testing the Group class" do
    it "should group name be present" do
      @group.name = ""
      expect(@group).to be_invalid
    end
  end
end

RSpec.describe Group, type: :model do
  context "when testing with the Group class" do
    it "should group name be unique" do
      duplicated_group = @group.dup
      duplicated_group.name = @group.name
      @group.save
      expect(duplicated_group).to be_invalid
    end
  end
end