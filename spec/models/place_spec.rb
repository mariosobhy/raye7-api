require 'spec_helper'

RSpec.configure do |config|
  config.before(:example) {
    @place = Place.new(name: "Maadi",latitude: 29.9627,longitude: 31.2769)
  }
end

RSpec.describe Place, type: :model do
  context "When testing the Place class" do
    it "should place name be present" do
      @place.name = ""
      expect(@place).to be_invalid
    end
  end
end

RSpec.describe Place, type: :model do
  context "When testing the Place class" do
    it "should place name be unique" do
      duplicated_place = @place.dup
      duplicated_place.name = @place.name
      @place.save
      expect(duplicated_place).to be_invalid
    end
  end
end
