require 'spec_helper'

RSpec.describe PlacesController, type: :controller do

  let(:valid_attributes) {
    skip("Add a hash of attributes valid for your model")
  }

  let(:invalid_attributes) {
    skip("Add a hash of attributes invalid for your model")
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # PlacesController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      place = Place.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      place = Place.create! valid_attributes
      get :show, params: {id: place.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Place" do
        expect {
          post :create, params: {place: valid_attributes}, session: valid_session
        }.to change(Place, :count).by(1)
      end

      it "renders a JSON response with the new place" do

        post :create, params: {place: valid_attributes}, session: valid_session
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json')
        expect(response.location).to eq(place_url(Place.last))
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new place" do

        post :create, params: {place: invalid_attributes}, session: valid_session
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested place" do
        place = Place.create! valid_attributes
        put :update, params: {id: place.to_param, place: new_attributes}, session: valid_session
        place.reload
        skip("Add assertions for updated state")
      end

      it "renders a JSON response with the place" do
        place = Place.create! valid_attributes

        put :update, params: {id: place.to_param, place: valid_attributes}, session: valid_session
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json')
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the place" do
        place = Place.create! valid_attributes

        put :update, params: {id: place.to_param, place: invalid_attributes}, session: valid_session
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested place" do
      place = Place.create! valid_attributes
      expect {
        delete :destroy, params: {id: place.to_param}, session: valid_session
      }.to change(Place, :count).by(-1)
    end
  end

end
