# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Group.create([{name: "Axis"},
             {name: "Allies"}])

Place.create([{name: 'Maadi', longitude: '31.2769', latitude: '29.9627'},
              {name: 'Zamalek', longitude: '31.2197', latitude: '30.0609'},
              {name: 'Smart Village', longitude: '31.0179', latitude: '30.0732'},
              {name: 'New Cairo', longitude: '31.4913', latitude: '30.0074'}])
User.create([{firstname: 'Mario',lastname: 'Sobhy', phonenumber: '01011676125', group_id:1,home_place_id:1,work_place_id:2},
             {firstname: 'Joseph',lastname: 'Botrous', phonenumber: '01208397770', group_id:1,home_place_id:2,work_place_id:1},
             {firstname: 'Nancy',lastname: 'Atef', phonenumber: '01017882201', group_id:1,home_place_id:3,work_place_id:2}])
Trip.create([{driver_id:1,source_id:1,destination_id:2,departuretime:Time.now.utc,seats:5},
             {driver_id:2,source_id:2,destination_id:1,departuretime:Time.now.utc,seats:4}])
UserTrip.create([{trip_id: 1,user_ids: [1]},
                 {trip_id: 2,user_ids: [1,2]}])