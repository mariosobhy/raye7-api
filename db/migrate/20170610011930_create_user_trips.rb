class CreateUserTrips < ActiveRecord::Migration[5.0]
  def change
    create_table :user_trips do |t|
      t.references :trip, foreign_key: true
      t.text :user_ids

      t.timestamps
    end
  end
end
