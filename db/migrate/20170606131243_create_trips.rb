class CreateTrips < ActiveRecord::Migration[5.0]
  def change
    create_table :trips do |t|
      t.belongs_to :driver
      t.belongs_to :source
      t.belongs_to :destination
      t.datetime :departuretime
      t.integer :seats

      t.timestamps
    end
  end
end
