class AddWorkAndHomePlaceAssociations < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :home_place_id, :integer
    add_column :users, :work_place_id, :integer
  end
end
