class TripsController < ApplicationController
  before_action :set_trip, only: [:show, :update]

  # GET /trips
  # GET /trips.json
  def index
    @trips = Trip.all.where(driver_id: params[:user_id])
  end

  # GET /trips/user_id.json
  def userTrips
    @trips = Trip.all
    @matched_trips = @trips.find_by(driver_id: 1)
  end

  # GET /trips/Y
  def showUserTrips
    @user_trip = UserTrip.find(params[:id])
  end


  #this will working only on seeds data because
  #still need to handle issue if join trip not have a UserTrip row
  # POST /trips/Y/join
  def join
    @user_trip = UserTrip.find(params[:id])
    @user_trip.user_ids << params[:user_ids]
    @user_trip.save
    if @user_trip.save
      render :join, status: :created
    else
      render json: @user_trip.errors, status: :unprocessable_entity
    end
  end

  def leave
    @user_trip = UserTrip.find(params[:id])
    @user_trip.user_ids.delete(params[:user_ids])
    @user_trip.save
    if @user_trip.save
      render :join, status: :created
    else
      render json: @user_trip.errors, status: :unprocessable_entity
    end
  end

  # GET /trips/1
  # GET /trips/1.json
  def show
  end

  # POST /trips
  # POST /trips.json
  def create
    @trip = Trip.new(trip_params)
    if @trip.save
      render :show, status: :created   #, location: @trip
    else
      render json: @trip.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /trips/1
  # PATCH/PUT /trips/1.json
  def update
    if @trip.update(trip_params)
      render :show, status: :ok , location: @trip
    else
      render json: @trip.errors, status: :unprocessable_entity
    end
  end

  # DELETE /trips/1
  # DELETE /trips/1.json
  def destroy
    @trips = Trip.all
    @trip = @trips.find_by(params[:user_id])
    if @trip.destroy
      render :show, status: :created   #, location: @trip
    else
      render json: @trip.errors, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trip
      @trip = Trip.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trip_params
      params.require(:trip).permit(:driver_id,:source_id,:destination_id,:departuretime,:seats)
    end
    def user_trips_params
      params.require(:UserTrip).permit(:user_ids)
    end
end
