class Place < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  has_one :home_user, class_name: "User", foreign_key: "home_place_id"
  has_one :work_user, class_name: "User", foreign_key: "work_place_id"
end
