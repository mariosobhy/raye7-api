class UserTrip < ApplicationRecord
  belongs_to :trip
  serialize :user_ids
end
