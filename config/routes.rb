Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get  '/groups',  to: 'groups#index'   #Done
  get  '/places',  to: 'places#index'   #Done
  get  '/users',   to: 'users#index'    #Done
  get  '/trips',   to: 'trips#index'    #Done
  get  '/trips/:id',  to: 'trips#showUserTrips'  #Done
  get 'trips/:id', to: 'trips#index'    #Done
  post '/trips/:id/join',  to: 'trips#join'     #Done
  post '/trips/:id/leave', to: 'trips#leave'    #Done
  post '/groups',  to: 'groups#create'    #Done
  post '/places',  to: 'places#create'    #Done
  post '/users',   to: 'users#create'     #Done
  post '/trips',   to: 'trips#create'     #Done
  resources :groups
  resource :places
  resource :users
  resource :trips
end
